const db = require('../utils/db')
const admin = require('firebase-admin')
const sharp = require('sharp')
const shortid = require('shortid')
const serviceAccount = require("../q-media.json")
const resolutions = [1024, 640, 320]

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://q-media-f1346.firebaseio.com"
})

const defaultStorage = admin.storage()
const bucket = defaultStorage.bucket('gs://q-media-f1346.appspot.com')

module.exports = {
  Mutation: {
    uploadMedia: async (_parent, args, _context, _info) => {
      const { uploadMediaInput } = args
      var mediaURLs = []
      const mediaId = shortid.generate()
      const uploadPromises = resolutions.map(async resolution => {
        const fileName = `${uploadMediaInput.name}_${mediaId}_${resolution}.${uploadMediaInput.contentType}`
        const image = await sharp(Buffer.from(uploadMediaInput.media.split(',')[1], 'base64'))
          .resize(resolution, resolution)
          .toBuffer()
        return bucket.file(fileName).save(image).then(async () => {
          await bucket.file(fileName).makePublic().then(async () => {
            await bucket.file(fileName).getSignedUrl({
              action: 'read',
              expires: '01-01-2100'
            }).then(signedUrls => {
              mediaURLs.push(signedUrls[0].split('?')[0])
            })
          })
        })
      })

      await Promise.all(uploadPromises)    
      delete uploadMediaInput.media
      uploadMediaInput.name = `${uploadMediaInput.name}_${mediaId}`
      uploadMediaInput.mediaURLs = mediaURLs
      const uploadedMedia = await db.collection('q-media').insertOne(uploadMediaInput)
      delete uploadedMedia.ops[0]._id
      return uploadedMedia.ops[0]

    },
    deleteMedia: async (_parent, args, _context, _info) => {
      const { deleteMediaInput } = args
      const media = await db.collection('q-media').findOne({name: deleteMediaInput.name})
      bucket.deleteFiles({prefix: media.name}, (err)=>{
        if(err){
          return null
        }
      })
      const deletedMedia = await db.collection('q-media').deleteOne({ name: deleteMediaInput.name })
      return null
    }
  }
}
